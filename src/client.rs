use crate::{
    error::{Error, Result},
    model::DumpResponse,
};
use futures::{
    compat::Future01CompatExt,
};
use log::debug;
use reqwest::{
    r#async::{Body, Client as ReqwestClient},
    header::{AUTHORIZATION, USER_AGENT, HeaderValue},
};
use std::sync::Arc;

pub struct Client {
    auth: String,
    host: String,
    https: bool,
    inner: Arc<ReqwestClient>,
}

impl Client {
    pub fn new(
        client: Arc<ReqwestClient>,
        https: bool,
        host: impl Into<String>,
        auth: impl Into<String>,
    ) -> Self {
        Self {
            auth: auth.into(),
            host: host.into(),
            inner: client,
            https,
        }
    }

    pub async fn dump<'a>(
        &'a self,
        body: impl Into<Vec<u8>> + 'a,
    ) -> Result<DumpResponse> {
        debug!("Preparing dump request");
        let url = http_client_base::url(self.https, &self.host, "/", None)?;
        debug!("Created url: {}", url);
        let headers = http_client_base::headers(vec![
            (AUTHORIZATION, HeaderValue::from_str(&self.auth)?),
        ])?;
        debug!("Created headers: {:?}", headers);

        debug!("Sending dump request");
        let mut resp = await!(self
            .inner
            .post(url)
            .headers(headers)
            .body(Body::from(body.into()))
            .send()
            .compat())?;
        debug!("Got dump response; deserializing");

        let model = await!(resp.json().compat())?;
        debug!("Deserialized dump response");

        Ok(model)
    }

    // Host is needed for temporary paste.dabbot.org support.
    pub async fn retrieve<'a>(
        &'a self,
        uuid: impl AsRef<str> + 'a,
        host: impl AsRef<str> + 'a,
    ) -> Result<Vec<u8>> {
        debug!("Preparing dump retrieve request");
        let path = format!("/{}", uuid.as_ref());
        let url = http_client_base::url(self.https, host.as_ref(), path, None)?;
        debug!("Created url: {}", url);

        let headers = http_client_base::headers(vec![
            (AUTHORIZATION, HeaderValue::from_str(&self.auth)?),
            (USER_AGENT, HeaderValue::from_static("dabbot")),
        ])?;
        debug!("Created headers: {:?}", headers);

        debug!("Sending dump retrieve request");
        let resp = await!(self
            .inner
            .get(url)
            .headers(headers)
            .send()
            .compat())?;
        debug!("Got dump retrieve response; deserializing");

        let bytes = await!(http_client_base::response_bytes::<Error>(resp))?;
        debug!("Deserialized dump retrieve response");

        Ok(bytes)
    }
}
