use http::header::InvalidHeaderValue;
use reqwest::{Error as ReqwestError, UrlError};
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
};

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    /// An error while parsing an `http` header value.
    InvalidHeaderValue(InvalidHeaderValue),
    /// An error from the `reqwest` crate.
    Reqwest(ReqwestError),
    /// An error from the `url` crate while parsing a URL.
    Url(UrlError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::InvalidHeaderValue(ref inner) => inner.description(),
            Error::Reqwest(ref inner) => inner.description(),
            Error::Url(ref inner) => inner.description(),
        }
    }
}

impl From<InvalidHeaderValue> for Error {
    fn from(err: InvalidHeaderValue) -> Self {
        Error::InvalidHeaderValue(err)
    }
}

impl From<UrlError> for Error {
    fn from(err: UrlError) -> Self {
        Error::Url(err)
    }
}

impl From<ReqwestError> for Error {
    fn from(err: ReqwestError) -> Self {
        Error::Reqwest(err)
    }
}
